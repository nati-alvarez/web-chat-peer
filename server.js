const express = require("express");
const server = express();
const http = require("http").createServer(server);
const peerServer = require("peer").ExpressPeerServer;


//peer server
server.use("/peerjs", peerServer(http, {debug: true}));

http.listen(443, console.log("peer server running here"));